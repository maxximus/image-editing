#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <QtGui/QImage>
#include <bb/cascades/AbstractPane>

using namespace bb::cascades;
namespace bb
{
    namespace cascades
    {
        class Application;
        class LocaleHandler;
    }
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class ApplicationUI : public QObject
{
    Q_OBJECT
public:
    ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI() { }
    void setImageView(QImage *image);
    Q_INVOKABLE QImage *greyScale(QImage * origin);
    Q_INVOKABLE QImage *brightness(int delta, QImage * origin);
    Q_INVOKABLE QImage *warm(int delta, QImage * origin);
    Q_INVOKABLE QImage *cool(int delta, QImage * origin);
    Q_INVOKABLE QImage *saturation(int delta, QImage * origin);
    Q_INVOKABLE void setGrey();
    Q_INVOKABLE void setBright();
    Q_INVOKABLE void setWarm();
    Q_INVOKABLE void setCool();
    Q_INVOKABLE void setSaturation();
    Q_INVOKABLE void saveImage();
private slots:
    void onSystemLanguageChanged();
private:
    AbstractPane *root;
    QTranslator* m_pTranslator;
    bb::cascades::LocaleHandler* m_pLocaleHandler;
    QImage* _image;
    QImage* _newImage;
};

#endif /* ApplicationUI_HPP_ */
