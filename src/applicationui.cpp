#include "applicationui.hpp"
#include <bb/ImageData>
#include <bb/cascades/Image>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/ImageView>


using namespace bb::cascades;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
        QObject(app)
{
    // prepare the localization
    m_pTranslator = new QTranslator(this);
    m_pLocaleHandler = new LocaleHandler(this);
    if(!QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()))) {
        // This is an abnormal situation! Something went wrong!
        // Add own code to recover here
        qWarning() << "Recovering from a failed connect()";
    }
    // initial load
    onSystemLanguageChanged();

    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    qml->setContextProperty("_app", this);

    // Create root object for the UI
    root = qml->createRootObject<AbstractPane>();

    // Set created root object as the application scene
    app->setScene(root);
    // Retrieve the path to the app's working directory
    QString workingDir = QDir::currentPath();
    QString filePathWithName = QDir::currentPath() + "/app/native/assets/sample.jpg";
    _image = new QImage(filePathWithName);

    setImageView(_image);
}

void ApplicationUI::onSystemLanguageChanged()
{
    QCoreApplication::instance()->removeTranslator(m_pTranslator);
    // Initiate, load and install the application translation files.
    QString locale_string = QLocale().name();
    QString file_name = QString("SampleImage_%1").arg(locale_string);
    if (m_pTranslator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(m_pTranslator);
    }
}

void ApplicationUI::setImageView(QImage *image) {
	ImageView* imageView = root->findChild<ImageView*>("image");
	QImage swappedImage = image->rgbSwapped();
	const bb::ImageData imageData = bb::ImageData::fromPixels(swappedImage.bits(), bb::PixelFormat::RGBX, swappedImage.width(), swappedImage.height(), swappedImage.bytesPerLine());
	bb::cascades::Image m_image = bb::cascades::Image(imageData);
//	    Image m_image = Image(filePathWithName);
	imageView->setImage(m_image);
}

Q_INVOKABLE QImage * ApplicationUI::greyScale(QImage * origin){
    QImage * newImage = new QImage(origin->width(), origin->height(), QImage::Format_ARGB32);

    QColor oldColor;

    for(int x = 0; x<newImage->width(); x++){
        for(int y = 0; y<newImage->height(); y++){
            oldColor = QColor(origin->pixel(x,y));
            int average = (oldColor.red()+oldColor.green()+oldColor.blue())/3;
            newImage->setPixel(x,y,qRgb(average,average,average));
        }
    }

    return newImage;
}

Q_INVOKABLE QImage * ApplicationUI::brightness(int delta, QImage * origin){
    QImage * newImage = new QImage(origin->width(), origin->height(), QImage::Format_ARGB32);

    QColor oldColor;
    int r,g,b;

    for(int x=0; x<newImage->width(); x++){
        for(int y=0; y<newImage->height(); y++){
            oldColor = QColor(origin->pixel(x,y));

            r = oldColor.red() + delta;
            g = oldColor.green() + delta;
            b = oldColor.blue() + delta;

            //we check if the new values are between 0 and 255
            r = qBound(0, r, 255);
            g = qBound(0, g, 255);
            b = qBound(0, b, 255);

            newImage->setPixel(x,y, qRgb(r,g,b));
        }
    }

    return newImage;
}

Q_INVOKABLE QImage * ApplicationUI::warm(int delta, QImage * origin){
    QImage *newImage = new QImage(origin->width(), origin->height(), QImage::Format_ARGB32);

    QColor oldColor;
    int r,g,b;

    for(int x=0; x<newImage->width(); x++){
        for(int y=0; y<newImage->height(); y++){
            oldColor = QColor(origin->pixel(x,y));

            r = oldColor.red() + delta;
            g = oldColor.green() + delta;
            b = oldColor.blue();

            //we check if the new values are between 0 and 255
            r = qBound(0, r, 255);
            g = qBound(0, g, 255);

            newImage->setPixel(x,y, qRgb(r,g,b));
        }
    }

    return newImage;
}

Q_INVOKABLE QImage * ApplicationUI::cool(int delta, QImage * origin){
    QImage *newImage = new QImage(origin->width(), origin->height(), QImage::Format_ARGB32);

    QColor oldColor;
    int r,g,b;

    for(int x=0; x<newImage->width(); x++){
        for(int y=0; y<newImage->height(); y++){
            oldColor = QColor(origin->pixel(x,y));

            r = oldColor.red();
            g = oldColor.green();
            b = oldColor.blue()+delta;

            //we check if the new value is between 0 and 255
            b = qBound(0, b, 255);

            newImage->setPixel(x,y, qRgb(r,g,b));
        }
    }

    return newImage;
}

Q_INVOKABLE QImage * ApplicationUI::saturation(int delta, QImage * origin){
    QImage * newImage = new QImage(origin->width(), origin->height(), QImage::Format_ARGB32);

    QColor oldColor;
    QColor newColor;
    int h,s,l;

    for(int x=0; x<newImage->width(); x++){
        for(int y=0; y<newImage->height(); y++){
            oldColor = QColor(origin->pixel(x,y));

            newColor = oldColor.toHsl();
            h = newColor.hue();
            s = newColor.saturation()+delta;
            l = newColor.lightness();

            //we check if the new value is between 0 and 255
            s = qBound(0, s, 255);

            newColor.setHsl(h, s, l);

            newImage->setPixel(x, y, qRgb(newColor.red(), newColor.green(), newColor.blue()));
        }
    }

    return newImage;
}

Q_INVOKABLE void ApplicationUI::setGrey() {
	QImage* newImage = greyScale(_image);
	_newImage = newImage;
	setImageView(newImage);
}

Q_INVOKABLE void ApplicationUI::setBright() {
	QImage* newImage = brightness(30,_image);
	_newImage = newImage;
	setImageView(newImage);
}

Q_INVOKABLE void ApplicationUI::setWarm() {
	QImage* newImage = warm(30,_image);
	_newImage = newImage;
	setImageView(newImage);
}

Q_INVOKABLE void ApplicationUI::setCool() {
	QImage* newImage = cool(30,_image);
	_newImage = newImage;
	setImageView(newImage);
}

Q_INVOKABLE void ApplicationUI::setSaturation() {
	QImage* newImage = saturation(5,_image);
	_newImage = newImage;
	setImageView(newImage);
}

Q_INVOKABLE void ApplicationUI::saveImage() {
	// Retrieve the path to the app's working directory
	QString workingDir = QDir::currentPath();
	QUrl file = QUrl("file://" + workingDir + "/shared/camera/newImage.jpg");
	_newImage->save("/accounts/1000/shared/camera/newImage.jpg");
}
