APP_NAME = SampleImage

CONFIG += qt warn_on cascades10
QT -= gui
LIBS   += -lbbdata

LIBS += -lbb

LIBS += -lbbplatformbbm
LIBS += -lbbsystem
include(config.pri)
