import bb.cascades 1.0
import bb.system 1.0
Page {
    Container {
        layout: DockLayout {
            
        }
        ImageView {
            id: image
            objectName: "image"
//            imageSource: "asset:///sample.jpg"
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center

        }
    }
    attachedObjects: [
        SystemToast {
            id: myQmlToast
            body: "File save in /accounts/1000/shared/camera/"
        }
    ]
    actions: [
        ActionItem {
            title: "Grey Scale"
            onTriggered: {
                _app.setGrey()
            }
        },
        ActionItem {
            title: "Brightness"
            onTriggered: {
                _app.setBright()
            }
        },
        ActionItem {
            title: "Warm"
            onTriggered: {
                _app.setWarm()
            }
        },
        ActionItem {
            title: "Cool"
            onTriggered: {
                _app.setCool()
            }
        },
        ActionItem {
            title: "Saturation"
            onTriggered: {
                _app.setSaturation()
            }
        },
        ActionItem {
            title: "Save"
            ActionBar.placement: ActionBarPlacement.OnBar
            onTriggered: {
                _app.saveImage();
                myQmlToast.show();
            }
        }
    ]
}
